USE [Tickets]
GO
/****** Object:  Table [dbo].[Comments]    Script Date: 2/8/2017 7:43:39 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Comments](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CommentedBy] [int] NULL,
	[TicketId] [int] NOT NULL,
	[CommentedDate] [datetime] NULL,
 CONSTRAINT [PK_Comments] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Roles]    Script Date: 2/8/2017 7:43:39 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Roles](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RoleName] [varchar](500) NOT NULL,
 CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TicketCategory]    Script Date: 2/8/2017 7:43:39 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TicketCategory](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CategoryName] [varchar](50) NULL,
	[ParentCategoryId] [int] NULL,
 CONSTRAINT [PK_TicketCategory] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tickets]    Script Date: 2/8/2017 7:43:39 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tickets](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](50) NULL,
	[Description] [nvarchar](50) NULL,
	[AssignedBy] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[AssignedTo] [int] NOT NULL,
	[StatusId] [int] NOT NULL,
	[CategoryId] [int] NOT NULL,
 CONSTRAINT [PK_Tickets] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TicketStatus]    Script Date: 2/8/2017 7:43:39 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TicketStatus](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[StatusName] [varchar](50) NOT NULL,
 CONSTRAINT [PK_TicketStatus] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Users]    Script Date: 2/8/2017 7:43:39 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Users](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [varchar](500) NOT NULL,
	[HashPassword] [varchar](500) NOT NULL,
	[Email] [varchar](500) NOT NULL,
	[FirstName] [varchar](500) NOT NULL,
	[LastName] [varchar](500) NOT NULL,
	[UserStatusId] [int] NOT NULL,
 CONSTRAINT [PK_Users_1] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[UsersRole]    Script Date: 2/8/2017 7:43:39 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UsersRole](
	[UserId] [int] NOT NULL,
	[RoleID] [int] NOT NULL,
 CONSTRAINT [PK_UsersRole] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[RoleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[UserStatus]    Script Date: 2/8/2017 7:43:39 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UserStatus](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserStatus] [varchar](50) NOT NULL,
 CONSTRAINT [PK_UserStatus] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Roles] ON 

GO
INSERT [dbo].[Roles] ([Id], [RoleName]) VALUES (1, N'Admin')
GO
INSERT [dbo].[Roles] ([Id], [RoleName]) VALUES (2, N'Users')
GO
SET IDENTITY_INSERT [dbo].[Roles] OFF
GO
SET IDENTITY_INSERT [dbo].[TicketCategory] ON 

GO
INSERT [dbo].[TicketCategory] ([Id], [CategoryName], [ParentCategoryId]) VALUES (1, N'Software', NULL)
GO
INSERT [dbo].[TicketCategory] ([Id], [CategoryName], [ParentCategoryId]) VALUES (2, N'Hardware', NULL)
GO
INSERT [dbo].[TicketCategory] ([Id], [CategoryName], [ParentCategoryId]) VALUES (3, N'Asp.net', 1)
GO
INSERT [dbo].[TicketCategory] ([Id], [CategoryName], [ParentCategoryId]) VALUES (4, N'SQL', 1)
GO
INSERT [dbo].[TicketCategory] ([Id], [CategoryName], [ParentCategoryId]) VALUES (5, N'MVC', 3)
GO
INSERT [dbo].[TicketCategory] ([Id], [CategoryName], [ParentCategoryId]) VALUES (6, N'C#', 3)
GO
INSERT [dbo].[TicketCategory] ([Id], [CategoryName], [ParentCategoryId]) VALUES (7, N'Stored procedure', 4)
GO
INSERT [dbo].[TicketCategory] ([Id], [CategoryName], [ParentCategoryId]) VALUES (8, N'Trigger', 4)
GO
INSERT [dbo].[TicketCategory] ([Id], [CategoryName], [ParentCategoryId]) VALUES (9, N'Views', 4)
GO
SET IDENTITY_INSERT [dbo].[TicketCategory] OFF
GO
SET IDENTITY_INSERT [dbo].[TicketStatus] ON 

GO
INSERT [dbo].[TicketStatus] ([Id], [StatusName]) VALUES (1, N'New')
GO
INSERT [dbo].[TicketStatus] ([Id], [StatusName]) VALUES (2, N'Open')
GO
INSERT [dbo].[TicketStatus] ([Id], [StatusName]) VALUES (3, N'Resolved')
GO
INSERT [dbo].[TicketStatus] ([Id], [StatusName]) VALUES (4, N'Closed')
GO
SET IDENTITY_INSERT [dbo].[TicketStatus] OFF
GO
SET IDENTITY_INSERT [dbo].[UserStatus] ON 

GO
INSERT [dbo].[UserStatus] ([Id], [UserStatus]) VALUES (1, N'Registered ')
GO
INSERT [dbo].[UserStatus] ([Id], [UserStatus]) VALUES (2, N'Verified')
GO
INSERT [dbo].[UserStatus] ([Id], [UserStatus]) VALUES (3, N'Locked')
GO
INSERT [dbo].[UserStatus] ([Id], [UserStatus]) VALUES (4, N'Blocked')
GO
SET IDENTITY_INSERT [dbo].[UserStatus] OFF
GO
ALTER TABLE [dbo].[Comments]  WITH CHECK ADD  CONSTRAINT [FK_Comments_Tickets] FOREIGN KEY([TicketId])
REFERENCES [dbo].[Tickets] ([Id])
GO
ALTER TABLE [dbo].[Comments] CHECK CONSTRAINT [FK_Comments_Tickets]
GO
ALTER TABLE [dbo].[Comments]  WITH CHECK ADD  CONSTRAINT [FK_Comments_Users] FOREIGN KEY([CommentedBy])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[Comments] CHECK CONSTRAINT [FK_Comments_Users]
GO
ALTER TABLE [dbo].[TicketCategory]  WITH CHECK ADD  CONSTRAINT [FK_TicketCategory_TicketCategory] FOREIGN KEY([ParentCategoryId])
REFERENCES [dbo].[TicketCategory] ([Id])
GO
ALTER TABLE [dbo].[TicketCategory] CHECK CONSTRAINT [FK_TicketCategory_TicketCategory]
GO
ALTER TABLE [dbo].[Tickets]  WITH CHECK ADD  CONSTRAINT [FK_Tickets_TicketCategory] FOREIGN KEY([CategoryId])
REFERENCES [dbo].[TicketCategory] ([Id])
GO
ALTER TABLE [dbo].[Tickets] CHECK CONSTRAINT [FK_Tickets_TicketCategory]
GO
ALTER TABLE [dbo].[Tickets]  WITH CHECK ADD  CONSTRAINT [FK_Tickets_TicketStatus] FOREIGN KEY([StatusId])
REFERENCES [dbo].[TicketStatus] ([Id])
GO
ALTER TABLE [dbo].[Tickets] CHECK CONSTRAINT [FK_Tickets_TicketStatus]
GO
ALTER TABLE [dbo].[Tickets]  WITH CHECK ADD  CONSTRAINT [FK_Tickets_Users] FOREIGN KEY([AssignedBy])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[Tickets] CHECK CONSTRAINT [FK_Tickets_Users]
GO
ALTER TABLE [dbo].[Tickets]  WITH CHECK ADD  CONSTRAINT [FK_Tickets_Users1] FOREIGN KEY([AssignedTo])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[Tickets] CHECK CONSTRAINT [FK_Tickets_Users1]
GO
ALTER TABLE [dbo].[Users]  WITH CHECK ADD  CONSTRAINT [FK_Users_UserStatus] FOREIGN KEY([UserStatusId])
REFERENCES [dbo].[UserStatus] ([Id])
GO
ALTER TABLE [dbo].[Users] CHECK CONSTRAINT [FK_Users_UserStatus]
GO
ALTER TABLE [dbo].[UsersRole]  WITH CHECK ADD  CONSTRAINT [FK_UsersRole_Roles] FOREIGN KEY([RoleID])
REFERENCES [dbo].[Roles] ([Id])
GO
ALTER TABLE [dbo].[UsersRole] CHECK CONSTRAINT [FK_UsersRole_Roles]
GO
ALTER TABLE [dbo].[UsersRole]  WITH CHECK ADD  CONSTRAINT [FK_UsersRole_Users] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[UsersRole] CHECK CONSTRAINT [FK_UsersRole_Users]
GO
USE [master]
GO
ALTER DATABASE [Tickets] SET  READ_WRITE 
GO
